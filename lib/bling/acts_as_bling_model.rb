module Bling
  module ActsAsBlingModel
    extend ActiveSupport::Concern

    module ClassMethods
      def acts_as_bling_model options = {}
        include Bling::ActsAsBlingModel::LocalInstanceMethods
      end
    end

    module LocalInstanceMethods
      def applicable
        self.respond_to? :sent_to_bling and self.respond_to? :to_bling_params and self.respond_to? :bling_class and self.respond_to? :should_update
      end

      def sent_to_bling?
        return nil unless self.applicable

        self.sent_to_bling.present?
      end

      def send_to_bling gerarnfe = nil
        return nil unless self.applicable

        if self.sent_to_bling?
          self.update_on_bling
        else
          self.create_on_bling gerarnfe: gerarnfe
        end
      end

      def create_on_bling extra_info = {}
        return nil unless self.applicable

        params = self.to_bling_params.merge(data_inclusao: Time.zone.now)

        params.merge!(gerarnfe: extra_info[:gerarnfe])
        response = bling_class.create(params)

        self.update_attributes(sent_to_bling: Time.zone.now, should_update: false)

        response
      end

      def update_on_bling
        return nil unless self.applicable

        params = self.to_bling_params.merge(data_alteracao: Time.zone.now)
        bling_class.update(self.edools_id, params)
        self.update_attributes(sent_to_bling: Time.zone.now, should_update: false)
      end

      def delete_on_bling
        return nil unless self.applicable

        bling_class.delete(self.edools_id)
        self.update_attributes(should_update: true, sent_to_bling: nil)
      end
    end
  end
end
