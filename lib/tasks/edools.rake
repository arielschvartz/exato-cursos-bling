namespace :edools do
  static_attributes = [:class_teachers, :categories, :school_products]
  statuses = [:paid, :waiting]

  static_attributes.each do |attr|
    task "import_#{attr}" => :environment do
      puts "\n\nIMPORTING #{attr} FROM EDOOLS\n\n"
      EdoolsImport.import(attr)
    end
  end

  task import_all_static: :environment do
    puts "IMPORTING ALL DATA FROM EDOOLS\n"
    static_attributes.each do |attr|
      Rake::Task["edools:import_#{attr}"].invoke
    end
  end

  statuses.each_with_index do |attr, i|
    task "import_#{attr}_payments" => :environment do
      puts "IMPORTING #{attr.to_s.upcase} PAYMENTS FROM EDOOLS\n\n"

      # last_waiting_import =
      # last_imports = statuses[0..i].map { EdoolsImport.where(finished: true, edools_model_type: "#{attr.capitalize}Payments").order('created_at DESC').limit(1).first }
      # last_import = last_imports.last

      # if attr == :waiting
      #   should_import_only_new = last_import.present? and last_import.import_data.empty?
      # elsif attr == :paid
      #   waiting_last_import = last_imports.first
      #   should_import_only_new = waiting_last_import.present? and waiting_last_import.import_data.empty?

      #   should_import_only_new = EdoolsImport.where(finished: true, edools_model_type: "PaidPayments").pluck(:import_data)
      # end

      last_import_data_present = EdoolsImport.last_waiting.present? and EdoolsImport.last_waiting_import_data.empty?

      import_time = nil

      if attr == :waiting
        if last_import_data_present
          import_time = EdoolsImport.last_waiting_time
        end
      elsif attr == :paid and EdoolsImport.last_paid.present?
        if last_import_data_present
          import_time = EdoolsImport.last_waiting_time
        else
          import_time = EdoolsImport.last_paid_time
        end
      end


      import = EdoolsImport.create(time: Time.zone.now, edools_model_type: "#{attr.capitalize}Payments")

      if import_time
        Payment.send("create_all_#{attr}_after", import_time)
        import.update_attributes(finished: true, import_data: Payment.send("edools_#{attr}_after_instances".to_sym))
      else
        Payment.send("create_all_#{attr}_edools_instances".to_sym)
        import.update_attributes(finished: true, import_data: Payment.send("edools_#{attr}_instances".to_sym))
      end
    end
  end

  task import_all_payments: :environment do
    puts "IMPORTING ALL PAYMENTS FROM EDOOLS\n\n"
    statuses.each do |attr|
      Rake::Task["edools:import_#{attr}_payments"].invoke
    end
  end

  task import_all: :environment do
    Rake::Task["edools:import_all_static"].invoke
    Rake::Task["edools:import_all_payments"].invoke
  end
end
