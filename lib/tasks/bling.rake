namespace :bling do
  classes = [:school_products]

  classes.each do |attr|
    task "send_#{attr}" => :environment do
      puts "\n\nSENDING #{attr} TO BLING\n\n"

      model_name = attr.to_s.singularize.camelize
      model_class = model_name.constantize

      model_class.to_send_to_bling.each do |instance|
        puts "SENDING #{model_name.upcase} WITH EDOOLS_ID #{instance.edools_id} TO BLING\n"
        instance.send_to_bling
      end
    end
  end

  task generate_invoices: :environment do
    puts "\n\nSENDING INVOICES TO BLING\n\n"

    Payment.to_send_to_bling.each do |p|
      p.send_to_bling
    end
  end
end
