class SchoolProduct < ActiveRecord::Base
  acts_as_paranoid
  acts_as_edools_model
  acts_as_bling_model

  has_many :school_product_class_teachers, dependent: :destroy
  has_many :class_teachers, through: :school_product_class_teachers

  has_many :school_product_categories, dependent: :destroy
  has_many :categories, through: :school_product_categories

  has_many :items, dependent: :destroy

  scope :to_send_to_bling, -> { where(should_update: true, published: true) }

  def applicable
    super and self.price.present?
  end

  def to_bling_params
    {
      codigo: self.edools_id,
      descricao: self.title,
      descricao_complementar: self.description,
      image_thumbnail: self.raw_hash['logo'],
      unidade: 'un',
      preco: (self.price / 100.0),
      class_fiscal: '9999.99.99',
      tipo: 'S',
      origem: '0'
    }
  end

  def bling_class
    Bling::Produto
  end

  def create_on_bling extra_info = {}
    extra_info = { data_inclusao: Time.zone.now }
    super
  end

  def update_on_bling extra_info = {}
    extra_info = { data_alteracao: Time.zone.now }
    super
  end
end
