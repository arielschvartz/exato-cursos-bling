class Item < ActiveRecord::Base
  acts_as_paranoid
  acts_as_edools_model

  belongs_to :order
  belongs_to :school_product

  after_save :save_school_product, if: -> { self.school_product.present? }

  def to_bling_params
    {
      # 'item' => {
      'codigo' => self.school_product.edools_id,
      'descricao' => self.school_product.title,
      'valor_unidade' => self.unit_price / 100.0,
      'vlr_unit' => self.unit_price / 100.0,
      'desconto_item' => self.discount / 100.0,
      'quantidade' => self.quantity,
      'qtde' => self.quantity,
      'class_fiscal' => '9999.99.99',
      'un' => 'un',
      'tipo' => 'P',
      'origem' => '0'
      # }
    }
  end

  def unit_price
    if self.total.present? and self.quantity.present?
      (self.total / self.quantity)
    else
      0
    end
  end

  def discount
    if self.unit_price > 0 and self.amount_to_pay.present?
      self.unit_price - self.amount_to_pay
    else
      0
    end
  end

  private

    def save_school_product
      self.school_product.save
    end
end
