class SchoolProductCategory < ActiveRecord::Base
  belongs_to :school_product
  belongs_to :category
end
