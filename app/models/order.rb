class Order < ActiveRecord::Base
  acts_as_paranoid
  acts_as_edools_model

  belongs_to :customer

  has_many :payments, dependent: :destroy
  has_many :items, dependent: :destroy

  def discount
    if self.total.present? and self.amount_to_pay.present?
      self.total - self.amount_to_pay
    else
      0
    end
  end
end
