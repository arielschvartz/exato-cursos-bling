class Object
  def metaclass
    class << self; self; end
  end
end

class EdoolsImport < ActiveRecord::Base
  serialize :import_data

  def self.import attr
    model_name = attr.to_s.singularize.camelize
    import = EdoolsImport.create(time: Time.zone.now, edools_model_type: model_name)

    model_class = model_name.constantize
    model_class.create_edools_instances

    import.update_attributes(finished: true, import_data: model_class.edools_instances)
  end

  [:waiting, :paid, :canceled].each do |attr|
    metaclass.instance_eval do
      define_method "last_#{attr}".to_sym do
        EdoolsImport.where(finished: true, edools_model_type: "#{attr.to_s.capitalize}Payments").order('created_at DESC').limit(1).first
      end

      [:import_data, :time].each do |attr2|
        define_method "last_#{attr}_#{attr2}".to_sym do
          obj = self.send("last_#{attr}".to_sym)

          if obj
            obj.send(attr2)
          else
            nil
          end
        end
      end
    end
  end
end
