class Customer < ActiveRecord::Base
  acts_as_paranoid
  acts_as_edools_model

  has_many :addresses, dependent: :destroy
  has_many :orders, dependent: :destroy
  has_many :payments, dependent: :destroy

  def name
    "#{first_name} #{last_name}"
  end

  def to_bling_params
    {
      'nome' => self.name,
      'email' => self.email,
      'endereco' => self.address.street,
      'numero' => self.address.number,
      'complemento' => self.address.complement,
      'cidade' => self.address.city,
      'bairro' => self.address.district,
      'cep' => self.address.zip_code,
      'uf' => self.address.state,
      'cnpj' => self.cpf,
      'cpf' => self.cpf,
      'cpf_cnpj' => self.cpf,
      'ie' => self.rg,
      'rg' => self.rg,
      'ie_rg' => self.rg,
      'tipo_pessoa' => 'F',
      'fone' => self.phone
    }
  end

  def address
    self.addresses.order('created_at DESC').limit(1).first
  end

  def cpf
    v = self.raw_hash['cpf']
    # "#{v[0..2]}.#{v[3..5]}.#{v[6..8]}-#{v[9..10]}"

    v
  end

  def rg
    self.raw_hash['rg']
  end

  def phone
    self.raw_hash['phone']
  end
end
