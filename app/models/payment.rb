class Object
  def metaclass
    class << self; self; end
  end
end

class Payment < ActiveRecord::Base
  acts_as_paranoid
  acts_as_edools_model
  acts_as_bling_model

  belongs_to :customer
  belongs_to :order
  delegate :items, to: :order, allow_nil: true

  scope :to_send_to_bling, -> { where(status: Edools::Payment.paid_statuses, should_update: true) }

  class << self
    [:waiting, :paid, :canceled].each do |attr|
      attr_accessor "edools_#{attr}_instances".to_sym
      attr_accessor "edools_#{attr}_after_instances".to_sym
    end
  end

  def self.get_all_after time
    if self.model_class and self.model_class.respond_to?(:all_after)
      model_class.send(:all_after, time)
    else
      nil
    end
  end

  def send_to_bling gerarnfe = nil
    response = Bling.post 'notafiscal', { 'pedido' => self.to_bling_params }

    if response['retorno'].present? and response['retorno']['notasfiscais'].present?
      # numero = response['retorno']['notasfiscais']['notafiscal']['numero']
      # idnota = response['retorno']['notasfiscais']['notafiscal']['idnotafiscal']

      # # Bling.post "notafiscal/#{numero}/#{idnota}"
      # # Bling.post ''

      # nfs = Bling.get 'notasfiscais?filters=situacao[1]'
      # nfs = nfs['retorno']['notasfiscais']['notafiscal']

      # if nfs.is_a? Array
      #   nfs = nfs.select { |x| x['numero'] == numero }
      #   nf = nfs.first
      # else
      #   nf = nfs
      # end

      # serie = nf['serie']

      # response = Bling.post "notafiscal/#{numero}/#{serie}?sendEmail=true&number=#{numero}&serie=#{serie}"
      
      # binding.pry

      self.update_attributes(sent_to_bling: Time.zone.now, should_update: false)
    end
  end

  def create hash = nil, gerarnfe = nil
      raise BlingError.new({'msg' => "#{self.error_variable.capitalize} não pode ficar em branco."}) unless self.can_create?

      hash ||= self.to_post_hash

      response = Bling.post("#{self.class.object_type}", hash, gerarnfe)

      unless response['retorno']['erros'].present?
        self.update_identifier(response) if self.respond_to? :update_identifier
        self.reload
      else
        raise_error_from_response response
      end
    end

  [:waiting, :paid, :canceled].each do |attr|
    metaclass.instance_eval do
      define_method "get_all_#{attr}".to_sym do
        instance_variable_name = "edools_#{attr}_instances"

        return self.send(instance_variable_name) if self.send(instance_variable_name).present?

        if self.model_class and self.model_class.respond_to?("all_#{attr}".to_sym)

          self.send("#{instance_variable_name}=", model_class.send("all_#{attr}".to_sym))
        else
          nil
        end
      end

      define_method "instantiate_all_#{attr}_edools_instances".to_sym do |save = false|
        instances = []
        edools_instances = []

        self.send("get_all_#{attr}".to_sym).each do |p|
          ep = Edools::Payment.find(p.id)
          edools_instances.push ep
          instances.push ep.to_model
          instances.last.save if save == true
        end

        self.send("edools_#{attr}_instances=".to_sym, edools_instances)

        instances
      end

      define_method "create_all_#{attr}_edools_instances".to_sym do
        self.send("instantiate_all_#{attr}_edools_instances".to_sym, true)
      end

      define_method "get_all_#{attr}_after".to_sym do |time|
        if self.model_class and self.model_class.respond_to?("all_#{attr}_after".to_sym)
          model_class.send("all_#{attr}_after".to_sym, time)
        else
          nil
        end
      end

      define_method "instantiate_all_#{attr}_after".to_sym do |time, save = false|
        instances = []
        edools_instances = []

        self.send("get_all_#{attr}_after".to_sym, time).each do |p|
          ep = Edools::Payment.find(p.id)
          edools_instances.push ep
          instances.push ep.to_model
          instances.last.save if save == true
        end

        self.send("edools_#{attr}_after_instances=".to_sym, edools_instances)

        instances
      end

      define_method "create_all_#{attr}_after".to_sym do |time|
        self.send("instantiate_all_#{attr}_after".to_sym, time, true)
      end
    end
  end

  def to_bling_params
    h = {
      desconto: self.order.discount / 100.0,
      total_produtos: self.order.items.pluck(:quantity).sum,
      total_venda: self.amount / 100.0,
      cliente: self.customer.to_bling_params,
      itens: self.items.map(&:to_bling_params),
      numero_loja: self.id
    }

    if self.bling_id.present?
      h.merge(numero: self.bling_id)
    else
      h
    end
  end

  def bling_class
    Bling::Pedido
  end

  after_save :save_customer, if: -> { self.customer.present? }
  after_save :save_order, if: -> { self.order.present? }

  private

    def save_customer
      unless self.customer.save
        puts "\n\n\nCOULD NOT SAVE CUSTOMER WITH EDOOLS_ID #{self.customer.edools_id} AND GUID #{self.customer.guid}!\n\n\n"
      end
    end

    def save_order
      unless self.order.save
        puts "\n\n\nCOULD NOT SAVE ORDER WITH EDOOLS_ID #{self.order.edools_id}!\n\n\n"
        self.items.map(&:save)
      end
    end
end
