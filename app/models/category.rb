class Category < ActiveRecord::Base
  acts_as_paranoid
  acts_as_edools_model

  has_many :school_product_categories, dependent: :destroy
  has_many :school_products, through: :school_product_categories
end
