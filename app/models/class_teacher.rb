class ClassTeacher < ActiveRecord::Base
  acts_as_paranoid
  acts_as_edools_model

  has_many :school_product_class_teachers, dependent: :destroy
  has_many :school_products, through: :school_product_class_teachers
end
