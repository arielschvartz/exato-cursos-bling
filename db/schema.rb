# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150706001120) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.integer  "edools_id"
    t.string   "street"
    t.string   "number"
    t.string   "complement"
    t.string   "district"
    t.string   "city"
    t.string   "state"
    t.string   "zip_code"
    t.text     "raw_hash"
    t.integer  "customer_id"
    t.datetime "deleted_at"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "addresses", ["customer_id"], name: "index_addresses_on_customer_id", using: :btree
  add_index "addresses", ["deleted_at"], name: "index_addresses_on_deleted_at", using: :btree
  add_index "addresses", ["edools_id"], name: "index_addresses_on_edools_id", using: :btree

  create_table "categories", force: :cascade do |t|
    t.integer  "edools_id"
    t.string   "title"
    t.boolean  "flag"
    t.text     "raw_hash"
    t.datetime "retrieved_from_edools"
    t.datetime "deleted_at"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "categories", ["deleted_at"], name: "index_categories_on_deleted_at", using: :btree
  add_index "categories", ["edools_id"], name: "index_categories_on_edools_id", using: :btree
  add_index "categories", ["retrieved_from_edools"], name: "index_categories_on_retrieved_from_edools", using: :btree

  create_table "class_teachers", force: :cascade do |t|
    t.integer  "edools_id"
    t.string   "full_name"
    t.text     "about"
    t.string   "photo_url"
    t.text     "raw_hash"
    t.datetime "retrieved_from_edools"
    t.datetime "deleted_at"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "class_teachers", ["deleted_at"], name: "index_class_teachers_on_deleted_at", using: :btree
  add_index "class_teachers", ["edools_id"], name: "index_class_teachers_on_edools_id", using: :btree
  add_index "class_teachers", ["retrieved_from_edools"], name: "index_class_teachers_on_retrieved_from_edools", using: :btree

  create_table "customers", force: :cascade do |t|
    t.integer  "edools_id"
    t.integer  "guid"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.text     "raw_hash"
    t.datetime "retrieved_from_edools"
    t.datetime "sent_to_bling"
    t.datetime "deleted_at"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "customers", ["deleted_at"], name: "index_customers_on_deleted_at", using: :btree
  add_index "customers", ["edools_id"], name: "index_customers_on_edools_id", using: :btree
  add_index "customers", ["email"], name: "index_customers_on_email", using: :btree
  add_index "customers", ["guid"], name: "index_customers_on_guid", using: :btree
  add_index "customers", ["retrieved_from_edools"], name: "index_customers_on_retrieved_from_edools", using: :btree
  add_index "customers", ["sent_to_bling"], name: "index_customers_on_sent_to_bling", using: :btree

  create_table "edools_imports", force: :cascade do |t|
    t.datetime "time"
    t.text     "import_data"
    t.boolean  "finished",          default: false
    t.string   "edools_model_type"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  add_index "edools_imports", ["time"], name: "index_edools_imports_on_time", using: :btree

  create_table "items", force: :cascade do |t|
    t.integer  "edools_id"
    t.integer  "total"
    t.integer  "amount_to_pay"
    t.integer  "quantity"
    t.text     "raw_hash"
    t.datetime "retrieved_from_edools"
    t.datetime "sent_to_bling"
    t.datetime "deleted_at"
    t.integer  "order_id"
    t.integer  "school_product_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "items", ["deleted_at"], name: "index_items_on_deleted_at", using: :btree
  add_index "items", ["edools_id"], name: "index_items_on_edools_id", using: :btree
  add_index "items", ["order_id"], name: "index_items_on_order_id", using: :btree
  add_index "items", ["retrieved_from_edools"], name: "index_items_on_retrieved_from_edools", using: :btree
  add_index "items", ["school_product_id"], name: "index_items_on_school_product_id", using: :btree
  add_index "items", ["sent_to_bling"], name: "index_items_on_sent_to_bling", using: :btree

  create_table "orders", force: :cascade do |t|
    t.integer  "edools_id"
    t.integer  "bling_id"
    t.string   "status"
    t.integer  "total"
    t.integer  "amount_to_pay"
    t.datetime "retrieved_from_edools"
    t.datetime "sent_to_bling"
    t.datetime "deleted_at"
    t.text     "raw_hash"
    t.integer  "customer_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "orders", ["customer_id"], name: "index_orders_on_customer_id", using: :btree
  add_index "orders", ["deleted_at"], name: "index_orders_on_deleted_at", using: :btree
  add_index "orders", ["edools_id"], name: "index_orders_on_edools_id", using: :btree
  add_index "orders", ["retrieved_from_edools"], name: "index_orders_on_retrieved_from_edools", using: :btree
  add_index "orders", ["sent_to_bling"], name: "index_orders_on_sent_to_bling", using: :btree

  create_table "payments", force: :cascade do |t|
    t.integer  "edools_id"
    t.integer  "bling_id"
    t.integer  "amount"
    t.integer  "gateway_fees"
    t.string   "payment_type"
    t.string   "status"
    t.datetime "retrieved_from_edools"
    t.datetime "sent_to_bling"
    t.boolean  "should_update"
    t.text     "raw_hash"
    t.datetime "deleted_at"
    t.integer  "customer_id"
    t.integer  "order_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "payments", ["customer_id"], name: "index_payments_on_customer_id", using: :btree
  add_index "payments", ["deleted_at"], name: "index_payments_on_deleted_at", using: :btree
  add_index "payments", ["edools_id"], name: "index_payments_on_edools_id", using: :btree
  add_index "payments", ["order_id"], name: "index_payments_on_order_id", using: :btree
  add_index "payments", ["retrieved_from_edools"], name: "index_payments_on_retrieved_from_edools", using: :btree
  add_index "payments", ["sent_to_bling"], name: "index_payments_on_sent_to_bling", using: :btree
  add_index "payments", ["status"], name: "index_payments_on_status", using: :btree

  create_table "school_product_categories", force: :cascade do |t|
    t.integer  "school_product_id"
    t.integer  "category_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "school_product_categories", ["category_id"], name: "index_school_product_categories_on_category_id", using: :btree
  add_index "school_product_categories", ["school_product_id"], name: "index_school_product_categories_on_school_product_id", using: :btree

  create_table "school_product_class_teachers", force: :cascade do |t|
    t.integer  "school_product_id"
    t.integer  "class_teacher_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "school_product_class_teachers", ["class_teacher_id"], name: "index_school_product_class_teachers_on_class_teacher_id", using: :btree
  add_index "school_product_class_teachers", ["school_product_id"], name: "index_school_product_class_teachers_on_school_product_id", using: :btree

  create_table "school_products", force: :cascade do |t|
    t.integer  "edools_id"
    t.integer  "bling_id"
    t.string   "title"
    t.text     "subtitle"
    t.text     "description"
    t.boolean  "published"
    t.text     "raw_hash"
    t.integer  "price"
    t.datetime "retrieved_from_edools"
    t.datetime "sent_to_bling"
    t.boolean  "should_update"
    t.boolean  "false"
    t.datetime "deleted_at"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "school_products", ["deleted_at"], name: "index_school_products_on_deleted_at", using: :btree
  add_index "school_products", ["edools_id"], name: "index_school_products_on_edools_id", using: :btree
  add_index "school_products", ["retrieved_from_edools"], name: "index_school_products_on_retrieved_from_edools", using: :btree
  add_index "school_products", ["sent_to_bling"], name: "index_school_products_on_sent_to_bling", using: :btree
  add_index "school_products", ["should_update"], name: "index_school_products_on_should_update", using: :btree

  add_foreign_key "addresses", "customers"
  add_foreign_key "items", "orders"
  add_foreign_key "items", "school_products"
  add_foreign_key "orders", "customers"
  add_foreign_key "payments", "customers"
  add_foreign_key "payments", "orders"
  add_foreign_key "school_product_categories", "categories"
  add_foreign_key "school_product_categories", "school_products"
  add_foreign_key "school_product_class_teachers", "class_teachers"
  add_foreign_key "school_product_class_teachers", "school_products"
end
