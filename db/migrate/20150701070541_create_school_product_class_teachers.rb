class CreateSchoolProductClassTeachers < ActiveRecord::Migration
  def change
    create_table :school_product_class_teachers do |t|
      t.references :school_product, index: true, foreign_key: true
      t.references :class_teacher, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
