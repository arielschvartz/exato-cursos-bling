class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.integer :edools_id

      t.string :street
      t.string :number
      t.string :complement
      t.string :district
      t.string :city
      t.string :state
      t.string :zip_code

      t.text :raw_hash

      t.references :customer, index: true, foreign_key: true

      t.datetime :deleted_at

      t.timestamps null: false
    end
    add_index :addresses, :edools_id
    add_index :addresses, :deleted_at
  end
end
