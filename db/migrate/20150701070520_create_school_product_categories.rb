class CreateSchoolProductCategories < ActiveRecord::Migration
  def change
    create_table :school_product_categories do |t|
      t.references :school_product, index: true, foreign_key: true
      t.references :category, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
