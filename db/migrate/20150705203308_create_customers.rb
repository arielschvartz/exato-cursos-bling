class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.integer :edools_id
      t.integer :guid

      t.string :first_name
      t.string :last_name
      t.string :email

      t.text :raw_hash

      t.datetime :retrieved_from_edools
      t.datetime :sent_to_bling

      t.datetime :deleted_at

      t.timestamps null: false
    end
    add_index :customers, :edools_id
    add_index :customers, :guid
    add_index :customers, :email
    add_index :customers, :retrieved_from_edools
    add_index :customers, :sent_to_bling
    add_index :customers, :deleted_at
  end
end
