class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.integer :edools_id

      t.string :title

      t.boolean :flag

      t.text :raw_hash

      t.datetime :retrieved_from_edools

      t.datetime :deleted_at

      t.timestamps null: false
    end

    add_index :categories, :deleted_at
    add_index :categories, :edools_id
    add_index :categories, :retrieved_from_edools
  end
end
