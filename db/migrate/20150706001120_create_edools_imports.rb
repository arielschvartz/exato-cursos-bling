class CreateEdoolsImports < ActiveRecord::Migration
  def change
    create_table :edools_imports do |t|
      t.datetime :time
      t.text :import_data, limit: 999999999

      t.boolean :finished, default: false

      t.string :edools_model_type

      t.timestamps null: false
    end
    add_index :edools_imports, :time
  end
end
