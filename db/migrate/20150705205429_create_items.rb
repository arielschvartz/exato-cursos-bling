class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.integer :edools_id

      t.integer :total
      t.integer :amount_to_pay
      t.integer :quantity

      t.text :raw_hash

      t.datetime :retrieved_from_edools
      t.datetime :sent_to_bling

      t.datetime :deleted_at

      t.references :order, index: true, foreign_key: true
      t.references :school_product, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :items, :edools_id
    add_index :items, :retrieved_from_edools
    add_index :items, :sent_to_bling
    add_index :items, :deleted_at
  end
end
