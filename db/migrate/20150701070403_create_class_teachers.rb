class CreateClassTeachers < ActiveRecord::Migration
  def change
    create_table :class_teachers do |t|
      t.integer :edools_id

      t.string :full_name
      t.text :about
      t.string :photo_url

      t.text :raw_hash

      t.datetime :retrieved_from_edools

      t.datetime :deleted_at

      t.timestamps null: false
    end

    add_index :class_teachers, :deleted_at
    add_index :class_teachers, :edools_id
    add_index :class_teachers, :retrieved_from_edools
  end
end
