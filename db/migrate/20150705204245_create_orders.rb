class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :edools_id
      t.integer :bling_id

      t.string :status

      t.integer :total
      t.integer :amount_to_pay

      t.datetime :retrieved_from_edools
      t.datetime :sent_to_bling

      t.datetime :deleted_at

      t.text :raw_hash

      t.references :customer, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :orders, :edools_id
    add_index :orders, :retrieved_from_edools
    add_index :orders, :sent_to_bling
    add_index :orders, :deleted_at
  end
end
