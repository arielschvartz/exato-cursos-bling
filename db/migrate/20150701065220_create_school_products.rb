class CreateSchoolProducts < ActiveRecord::Migration
  def change
    create_table :school_products do |t|
      t.integer :edools_id
      t.integer :bling_id

      t.string :title
      t.text :subtitle
      t.text :description

      t.boolean :published

      t.text :raw_hash

      t.integer :price

      t.datetime :retrieved_from_edools
      t.datetime :sent_to_bling

      t.boolean :should_update, false

      t.datetime :deleted_at

      t.timestamps null: false
    end

    add_index :school_products, :deleted_at
    add_index :school_products, :sent_to_bling
    add_index :school_products, :retrieved_from_edools

    add_index :school_products, :should_update
    add_index :school_products, :edools_id
  end
end
