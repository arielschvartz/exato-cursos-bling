class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.integer :edools_id
      t.integer :bling_id

      t.integer :amount
      t.integer :gateway_fees

      t.string :payment_type
      t.string :status

      t.datetime :retrieved_from_edools
      t.datetime :sent_to_bling

      t.boolean :should_update

      t.text :raw_hash

      t.datetime :deleted_at

      t.references :customer, index: true, foreign_key: true
      t.references :order, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :payments, :edools_id
    add_index :payments, :status
    add_index :payments, :retrieved_from_edools
    add_index :payments, :sent_to_bling
    add_index :payments, :deleted_at
  end
end
